import json
import pytest

from scientists.e_tokenizer.document_parser import document_parser
from utils.csv_string import list_to_csv_string

MD = """# tohle je čistě jen md
možná víceřádkový, ale to nevadí
někde bude konec"""

PARSED_MD = [(2, 'tohle', 'w', 'h1.2'),
             (8, 'je', 'w', 'h1.2'),
             (11, 'čistě', 'w', 'h1.2'),
             (17, 'jen', 'w', 'h1.2'),
             (21, 'md', 'w', 'h1.2'),
             (24, 'možná', 'w', 'p.24'),
             (30, 'víceřádkový', 'w', 'p.24'),
             (41, ',', '.', 'p.24'),
             (43, 'ale', 'w', 'p.24'),
             (47, 'to', 'w', 'p.24'),
             (50, 'nevadí', 'w', 'p.24'),
             (57, 'někde', 'w', 'p.24'),
             (63, 'bude', 'w', 'p.24'),
             (68, 'konec', 'w', 'p.24')]


def test_simple_md_doc():
    props, md = document_parser(MD)
    assert props == {}
    assert md == PARSED_MD


def test_doc_with_props():
    source_props = """* title: něco
* url: http://some.url
---
"""
    props, md = document_parser(source_props + MD)
    assert props == {'title': 'něco', 'url': 'http://some.url'}
    assert md == PARSED_MD


def test_doc_with_tag_in_props():
    source_props = """* title: něco
* url: http://some.url
* tags: abc, def, ghi
---
"""
    props, md = document_parser(source_props + MD)
    assert props == {'title': 'něco', 'url': 'http://some.url',
                     'tags': ['abc', 'def', 'ghi']}
    assert md == PARSED_MD


@pytest.mark.parametrize("filename", ('kamen', 'neptun'))
def test_data_file(filename: str):
    with open(f'data/{filename}.md') as f:
        content = f.read()

    props, tokens = document_parser(content)
    result_str = list_to_csv_string(tokens)

    with open(f'data/{filename}.tokens.csv') as f:
        expected = f.read()

    if result_str != expected:
        with open(f'data/{filename}.tokens.csv', 'w') as f:
            f.write(result_str)
    assert result_str == expected
