ENTITY_TYPES = {
    'general': 'Obecný',
    'construction': 'Stavba',
    'place': 'Místo',
    'country': 'Stát',
    'person': 'Osoba',
    'event': 'Událost',
    'group': 'Skupina',
    'entity': 'Instituce',
    'calendar': 'Datum/Čas',
    'topic': 'Téma',
    'role': 'Role',
}
