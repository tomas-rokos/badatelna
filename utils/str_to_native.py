def str_to_native(item: str):
    if '.' in item:
        try:
            float_value = float(item)
            return float_value
        except ValueError:
            pass
    else:
        try:
            int_value = int(item)
            return int_value
        except ValueError:
            pass

    return item
