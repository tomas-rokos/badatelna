# pylint: disable=R0903

class TokenType:
    # direct document token types
    UNKNOWN = ''
    WORD = 'w'
    NUMBER = 'n'
    SPACE = '-'
    PUNCTUATION = '.'
    SPECIALS = '$'

    # structure token types
    DOCUMENT = 'doc'
    PARAGRAPH = 'p'
    HEADER1 = 'h1'
    HEADER2 = 'h2'
    HEADER3 = 'h3'
    SENTENCE = 's'

    # entity token types
    CONTAINER = '#'
    DATETIME = 'dt'
    EVENT = 'ev'
    LEGAL_UNIT = 'lu'
    PERSON = 'pe'
    PLACE = 'pl'
    METADATA = 'md'

    # track
    TRACK = 't'
