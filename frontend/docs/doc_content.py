from crassum.react.pyreact_functional import pyreact_sprite, use_effect, use_state

from frontend.components.tokenized import tokenized


# language=css
STYLE = '''
:host {
    margin: 10px;
}

'''


def doc_content(props):
    doc = props['doc']

    return pyreact_sprite(
        ('div', {'class': STYLE}, [
            ('h2', None, [
                doc['props']['title']
            ]),
            (tokenized, {'tokens': doc['tokens'], 'hooks': True}),
        ])
    )
