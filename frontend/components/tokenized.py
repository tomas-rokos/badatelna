from crassum.react.pyreact_functional import pyreact_sprite, use_effect,\
    use_state


# language=css
STYLE = '''
:host {
}

:host.unselectable {
    user-select: none;
}

:host .token {
    display: inline-flex;
    flex-direction: column;
    padding: 0 3px;
    color: black; 
    border-bottom: 1px solid transparent;
}

:host strong {
    font-weight: revert;
    display: contents;
}

:host a {
    border-bottom: 1px solid grey;
}

:host h1 {
    margin: 5px 0;
    font-weight: bold;
    font-size: 140%;
}

:host h2 {
    margin: 5px 0;
    font-weight: bold;
    font-size: 110%;
}

:host h3 {
    margin: 5px 0;
    font-weight: bold;
    font-size: 100%;
}

:host p {
    flex-wrap: wrap;
    display: flex;
}

'''


def tokenized(props):
    tokens = props['tokens']
    start_idx = props['start'] or 0
    end_idx = props['end'] or -1
    hooks = props['hooks'] or False

    render_tokens = _render_level(tokens, '', start_idx, end_idx, hooks)

    return pyreact_sprite(
        ('div', {'class': STYLE}, render_tokens),
    )


def _render_level(tokens: list, level_prefix='', start=0, end=-1,
                  hooks=False) -> list:
    render_tokens = []
    prefix_len = len(level_prefix.split('.'))
    if prefix_len == 1:
        prefix_len = 0

    current_group = None
    current_tokens = []
    ln = len(tokens) if end == -1 else end + 1
    for token_idx in range(start, ln + 1):
        token = tokens[token_idx] if token_idx < ln else ('', '', '', 'x')
        parts = token[3].split('.')
        group = '.'.join(parts[:prefix_len+2])
        if group != current_group and len(current_tokens) > 0:
            sub_tokens = _render_level(current_tokens, current_group,
                                       0, -1, hooks)
            group_parts = current_group.split('.')
            render_tokens.append((group_parts[len(group_parts)-2], None,
                                  sub_tokens))
            current_tokens = []
        current_group = group
        if group == level_prefix:
            render_tokens.append(_render_token(token, hooks))
            continue
        current_tokens.append(token)

    return render_tokens


def _render_token(token: tuple, hooks: bool) -> tuple:
    token_props = {'key': token[0]}
    classes = ['token']
    if hooks:
        classes.append(f'token-{token[0]}')
    token_props['className'] = ' '.join(classes)
    return 'div', token_props, token[1]
