from frontend.components.tokenized import _render_level


def test_render_level_simple():
    tokens = [
        (2, 'Akce',     'w', 'h1.2'),
        (7, 'Neptun',   'w', 'h1.2'),
        (14, 'Akce',    'w', 'p.14'),
        (19, 'Neptun',  'w', 'p.14'),
        (26, 'je',      'w', 'p.14'),
        (29, 'krycí',   'w', 'p.14'),
        (35, 'název',   'w', 'p.14'),
    ]

    expected = [('h1', None, [
        ('div', {'className': 'token', 'key': 2}, 'Akce'),
        ('div', {'className': 'token', 'key': 7}, 'Neptun'),
    ]), ('p', None, [
        ('div', {'className': 'token', 'key': 14}, 'Akce'),
        ('div', {'className': 'token', 'key': 19}, 'Neptun'),
        ('div', {'className': 'token', 'key': 26}, 'je'),
        ('div', {'className': 'token', 'key': 29}, 'krycí'),
        ('div', {'className': 'token', 'key': 35}, 'název')
    ])]
    assert _render_level(tokens) == expected


def test_render_level_nested():
    tokens = [
        (2, 'Akce',     'w', 'h1.2'),
        (7, 'Neptun',   'w', 'h1.2'),
        (14, 'Akce',    'w', 'p.14.strong.14'),
        (19, 'Neptun',  'w', 'p.14.strong.14'),
        (26, 'je',      'w', 'p.14'),
        (29, 'krycí',   'w', 'p.14'),
        (35, 'název',   'w', 'p.14'),
    ]

    expected = [
        ('h1', None, [
            ('div', {'className': 'token', 'key': 2}, 'Akce'),
            ('div', {'className': 'token', 'key': 7}, 'Neptun'),
        ]),
        ('p', None, [
            ('strong', None, [
                ('div', {'className': 'token', 'key': 14}, 'Akce'),
                ('div', {'className': 'token', 'key': 19}, 'Neptun'),
            ]),
            ('div', {'className': 'token', 'key': 26}, 'je'),
            ('div', {'className': 'token', 'key': 29}, 'krycí'),
            ('div', {'className': 'token', 'key': 35}, 'název')
        ])]
    assert _render_level(tokens) == expected


def test_render_level_nested_2():
    tokens = [
        (2, 'Akce',     'w', 'h1.2'),
        (7, 'Neptun',   'w', 'h1.2'),
        (14, 'Akce',    'w', 'p.14'),
        (19, 'Neptun',  'w', 'p.14'),
        (26, 'je',      'w', 'p.14'),
        (29, 'krycí',   'w', 'p.14.strong.29'),
        (35, 'název',   'w', 'p.14.strong.29'),
    ]

    expected = [
        ('h1', None, [
            ('div', {'className': 'token', 'key': 2}, 'Akce'),
            ('div', {'className': 'token', 'key': 7}, 'Neptun'),
        ]),
        ('p', None, [
            ('div', {'className': 'token', 'key': 14}, 'Akce'),
            ('div', {'className': 'token', 'key': 19}, 'Neptun'),
            ('div', {'className': 'token', 'key': 26}, 'je'),
            ('strong', None, [
                ('div', {'className': 'token', 'key': 29}, 'krycí'),
                ('div', {'className': 'token', 'key': 35}, 'název')
            ]),
        ])]
    assert _render_level(tokens) == expected


def test_render_level_partial():
    tokens = [
        (2, 'Akce',     'w', 'h1.2'),
        (7, 'Neptun',   'w', 'h1.2'),
        (14, 'Akce',    'w', 'p.14.strong.14'),
        (19, 'Neptun',  'w', 'p.14.strong.14'),
        (26, 'je',      'w', 'p.14'),
        (29, 'krycí',   'w', 'p.14'),
        (35, 'název',   'w', 'p.14'),
    ]

    expected = [
        ('p', None, [
            ('strong', None, [
                ('div', {'className': 'token', 'key': 14}, 'Akce'),
                ('div', {'className': 'token', 'key': 19}, 'Neptun'),
            ]),
        ])]
    assert _render_level(tokens, '', 2, 3) == expected
