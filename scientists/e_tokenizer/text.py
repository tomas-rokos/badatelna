from scientists.e_tokenizer.token_type_enum import TokenType

breakers = [160, 32, 10, 13]
punctuations = ['.', ',', ';', '“', '„', '(', ')', '[', ']']


def text(txt: str):
    tokenized: list = []

    txt += chr(0)
    txt_len = len(txt)

    act_type = None
    act_string = ''
    act_start = 0

    for i in range(txt_len):
        curr_type = TokenType.UNKNOWN
        ch = txt[i]
        c = ord(ch)
        if c == -1:
            curr_type = TokenType.SPECIALS
        elif c in breakers:
            curr_type = TokenType.SPACE
        elif 65 <= c <= 90:
            curr_type = TokenType.WORD
        elif 97 <= c <= 122:
            curr_type = TokenType.WORD
        elif 48 <= c <= 57:
            curr_type = TokenType.NUMBER
        elif ch in punctuations:
            curr_type = TokenType.PUNCTUATION
        elif c >= 192:
            curr_type = TokenType.WORD

        if act_type is None:
            act_type = curr_type

        if act_type == curr_type:
            act_string += ch
            continue

        if act_type != TokenType.SPACE:
            if act_type == TokenType.NUMBER:
                act_string = int(act_string)
            tokenized.append((act_start, act_string, act_type))

        act_string = ch
        act_type = curr_type
        act_start = i

    return tokenized
