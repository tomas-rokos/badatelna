from crassum.react.components.route import Route
from crassum.react.pyreact_functional import pyreact_sprite, use_effect, use_state

from frontend.components.tokenized import tokenized
from frontend.datastore.datastore import Datastore

from scientists.k_streaks_detector.streaks_detector import streaks_detector
from scientists.n_timeliner.timeliner import timeliner


# language=css

STYLE = '''
:host {
}

:host .item {
    margin: 0 5px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    border-left: 5px solid grey;
    border-right: 1px solid grey;
    border-bottom: 1px solid grey;
}

:host .item:first-child {
    border-top: 1px solid grey;
}

:host .item .item-sub {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
}

:host .item .datetime {
    min-width: 100px;
    text-align: center;
    margin: auto 0;
}

:host .item .link {
    width: 50px;
    text-align: center;
    margin: auto 0;
    flex-shrink: 0;
    cursor: pointer;
}
'''


def streaks(props):
    def _on_click(source: dict):
        def _impl(e):
            props['scrollToHook'](source)
        return _impl

    datastore = Datastore.instance
    found_streaks = streaks_detector(datastore)
    timeline_streaks = timeliner(found_streaks)
    elems = []
    for item in timeline_streaks:
        doc = datastore.get_doc(item['source']['doc'])
        scroll_source = {
            'doc': item['source']['doc'],
            'token': doc['tokens'][item['source']['from']][0],
        }
        elems.append(
            ('div', 'item', [
                ('div', 'item-sub', [
                    ('div', 'datetime', item['timeline']),
                    (tokenized, {
                        'tokens': doc['tokens'],
                        'start': item['source']['from'],
                        'end': item['source']['to'],
                    }),
                ]),
                ('div', {'className': 'link',
                         'onClick': _on_click(scroll_source)}, '>>'),
             ])
        )

    return pyreact_sprite(
        ('div', {'class': STYLE, 'className': props['className']}, elems)
    )
