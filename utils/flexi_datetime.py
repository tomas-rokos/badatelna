
class FlexiDatetime:
    def __init__(self):
        self.day = None
        self.month = None
        self.year = None
        self.day_of_week = None
        self.hour = None
        self.minute = None
        self.second = None

    def copy(self):
        cp = FlexiDatetime()
        cp.day = self.day
        cp.month = self.month
        cp.year = self.year
        cp.day_of_week = self.day_of_week
        cp.hour = self.hour
        cp.minute = self.minute
        cp.second = self.second
        return cp

    def __repr__(self):
        ret = ''
        if self.year is not None or self.month is not None or\
                self.day is not None:
            parts = [
                str(self.year).zfill(4) if self.year is not None else '????',
            ]
            if self.month is not None:
                parts.append(str(self.month).zfill(2))
            elif self.day is not None:
                parts.append('??')
            if self.day is not None:
                parts.append(str(self.day).zfill(2))
            ret += '-'.join(parts)
        if self.hour is not None or self.minute is not None or \
                self.second is not None:
            parts = [
                str(self.hour).zfill(2) if self.hour is not None else '??',
                str(self.minute).zfill(2) if self.minute is not None else '??'
            ]
            if self.second is not None:
                parts.append(str(self.second).zfill(2))
            if len(ret) != 0:
                ret += 'T'
            ret += ':'.join(parts)
        return ret
