from scientists.e_tokenizer.markup import markup


def document_parser(doc: str):
    fragments = doc.split('---\n')
    if len(fragments) == 1:
        return {}, markup(doc)
    props = {}
    for prop in fragments[0].split('\n'):
        if prop[:2] != '* ':
            continue
        prop_parts = [item.strip() for item in prop[2:].split(':')]
        prop_val = ':'.join(prop_parts[1:])
        if prop_parts[0] in ('tags', ):
            prop_val = [item.strip() for item in prop_val.split(',')]
        props[prop_parts[0]] = prop_val
    return props, markup(fragments[1])
