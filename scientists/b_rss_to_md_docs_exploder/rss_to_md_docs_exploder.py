from datetime import datetime

from bs4 import BeautifulSoup

from scientists.b_rss_to_md_docs_exploder.construct_rss_guid import \
    construct_rss_guid

PATTERN = '* title: {title}\n' \
          '* url: {link}\n' \
          '* publish: {publish}\n' \
          '* guid: {guid}\n' \
          '---\n' \
          '# {title}\n' \
          '{description}'

PUBLISH_DATE_FORMAT = '%a, %d %b %Y %H:%M:%S %z'
PUBLISH_DATE_FORMAT_TZ_NAME = '%a, %d %b %Y %H:%M:%S %Z'


def rss_to_md_docs_exploder(content: bytes) -> [str]:
    parser = BeautifulSoup(content, 'xml')
    result = []
    for item in parser.findAll("item"):
        description_soup = BeautifulSoup(item.description.text, "html.parser")
        publish = item.pubDate.text
        date_format = PUBLISH_DATE_FORMAT if publish.endswith('0') \
            else PUBLISH_DATE_FORMAT_TZ_NAME
        publish = datetime.strptime(publish, date_format)
        guid = item.guid
        link_guid = None
        if guid is not None:
            if guid.attrs['isPermaLink'] == 'true':
                link_guid = guid.text
            else:
                guid = guid.text
        else:
            link_guid = item.link.text

        if link_guid:
            guid = construct_rss_guid(link_guid)

        params = {
            'title': item.title.text,
            'link': item.link.text,
            'description': description_soup.text.strip(),
            'publish': publish,
            'guid': guid,
        }

        result.append(PATTERN.format(**params))
    return result
