from crassum.react.components.route import Route
from crassum.react.materializecss.text_input import text_input
from crassum.react.pyreact_functional import pyreact_sprite, use_effect, use_state
from crassum.utils.random_string import random_string
from frontend.components.tokenized import tokenized

from frontend.datastore.datastore import Datastore
from frontend.workplace.streaks import streaks

from scientists.k_streaks_detector.streaks_detector import streaks_detector


# language=css
STYLE = '''
:host {
    height: 100vh;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
}

:host .toolbar {
    flex-grow: 0;
    flex-shrink: 0;
}

:host .toolbar input {
    width: calc(100% - 20px);
    box-sizing: border-box;
    outline-offset: 0;
    height: 30px;
    margin: 7px 10px;
}

:host .place-a,
:host .place-b {
    height: calc((100vh - 45px) / 2);
    overflow: scroll;
}

:host .place {
    height: calc(100vh - 45px);
    overflow: scroll;
}
'''


def workplace(props):
    def _on_input(e):
        if e.keyCode != 13:
            return
        console.log(e.currentTarget.value)

    elems = [
        ('div', 'toolbar', [
            ('input', {'className': 'browser-default',
                       'placeholder': 'Struktůrovaný dotaz',
                       'type': 'search',
                       'onKeyUp': _on_input},),
        ]),
    ]
    if True:
        elems.append((streaks, {'className': 'place',
                                'scrollToHook': props['scrollToHook']}))
    else:
        elems.extend([
            (streaks, 'place-a'),
            ('div', 'place-b', [('div', None, random_string(10))
                                for _ in range(30)]),
        ])
    return pyreact_sprite(
        ('div', {'class': STYLE, 'className': 'workplace'}, elems)
    )
