from utils.str_to_native import str_to_native


def line_list_to_csv_string(line_cells: list) -> str:
    return ','.join([_item_to_csv_cell(cell) for cell in line_cells])


def list_to_csv_string(source: list) -> str:
    return '\n'.join([line_list_to_csv_string(item) for item in source])


def _item_to_csv_cell(item) -> str:
    item = str(item).replace('"', '""')
    if ',' in item:
        item = '"' + item + '"'
    return item


def csv_string_to_list(source: str) -> list:
    lines = source.split('\n')
    result = []
    for line in lines:
        cells = []
        curr_cell = ''
        in_quoted = False
        for single_char in line:
            if single_char == '"' and (curr_cell == '' or in_quoted):
                in_quoted = bool(curr_cell == '')
            elif single_char == ',' and not in_quoted:
                cells.append(str_to_native(curr_cell))
                curr_cell = ''
            else:
                curr_cell += single_char
        cells.append(str_to_native(curr_cell))
        result.append(cells)
    return result
