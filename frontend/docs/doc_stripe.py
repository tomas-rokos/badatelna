from crassum.react.components.route import Route
from crassum.react.pyreact_functional import pyreact_sprite, use_effect, use_state


# language=css
STYLE = '''
:host {
    margin: 10px;
    border: 1px solid grey;
    border-radius: 5px;
    padding: 5px; 
}

:host .title {
    font-size: 150%;
    margin: 0;
    cursor: pointer;
}

'''


def doc_stripe(props):
    doc = props['doc']

    return pyreact_sprite(
        ('div', {'class': STYLE}, [
            ('h2', {'className': 'title', 'onClick': props['onClick']}, [
                doc['props']['title']
            ]),
            (Route, {'href': doc['props']['url']},[
                ('div', 'url', doc['props']['url'])
            ]),
            ('div', 'tags', ', '.join(doc['props']['tags'])),
        ])
    )
