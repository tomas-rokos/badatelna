from crassum.react.bootstrap import bootstrap
from crassum.react.pyreact_functional import pyreact_sprite, use_effect, \
    use_state
from crassum.js.navigation import navigate, on_navigation, get_location
from frontend.datastore.datastore import Datastore
from frontend.docs.docs import docs
from frontend.workplace.workplace import workplace
from utils.str_to_native import str_to_native


# language=css
STYLE = '''
:host {
    height: 100vh;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
}

:host .workplace {
    flex-grow: 1;
    flex-shrink: 1;
}

:host .docs {
    width: 500px; 
    flex-grow: 0;
    flex-shrink: 0;
    background-color: #eee;
}
'''


def _parse_search_url(loc):
    if len(loc) == 0:
        return {}
    search = loc[1:]
    search = search.split('&')
    result = {}
    for single in search:
        parts = single.split('=')
        if len(parts) == 1:
            result[parts[0]] = True
        else:
            result[parts[0]] = str_to_native(parts[1])

    return result


def _scroll_to_token(pos):
    def _impl():
        print(pos)
        elems = document.getElementsByClassName('token-' + str(pos))
        if len(elems) == 0:
            console.log('no element to scroll')
            return
        if len(elems) > 1:
            console.log(len(elems), ' element to scroll')
        workplace = document.getElementsByClassName('content')[0]
        workplace.scroll(0, elems[0].offsetTop - 50)

    return _impl


def application(props: dict):
    curr_location, set_curr_location = use_state(
        _parse_search_url(get_location().search))

    def _handle_location_change():
        def _on_navigation_change(loc):
            new_location = _parse_search_url(loc.search)
            set_curr_location(new_location)

        on_navigation(_on_navigation_change)
    use_effect(_handle_location_change, [])

    selected_doc, set_selected_doc = use_state('')

    def _handle_scroll_to_hook(source):
        set_selected_doc(source['doc'])
        setTimeout(_scroll_to_token(source['token']), 200)

    return pyreact_sprite(
        ('div', {'class': STYLE}, [
            (workplace, {'scrollToHook': _handle_scroll_to_hook}),
            (docs,  {'doc': selected_doc, 'setDoc': set_selected_doc}),
        ])
    )


async def main():
    datastore = Datastore()
    await datastore.load_docs()
    bootstrap(application, False)


main()
