from crassum.react.materializecss.button import button
from crassum.react.materializecss.form import form
from crassum.react.materializecss.text_area import text_area
from crassum.react.pyreact_functional import pyreact_sprite, use_effect, use_state

from frontend.datastore.datastore import Datastore
from frontend.docs.doc_content import doc_content
from frontend.docs.doc_stripe import doc_stripe
from frontend.docs.info import info


# language=css
STYLE = '''
:host {
    height: 100vh;
    display: flex;
    flex-direction: column;
    overflow: hidden;
}

:host .toolbar {
    margin: 10px;
}

:host .toolbar a {
    margin: 0 5px;
}

:host .content {
    overflow: scroll;
}

:host .editor-text-area {
    width: 1000px;
}

'''


def docs(props):
    doc_keys = Datastore.instance.get_doc_keys()

    show_edit, set_show_edit = use_state(False)

    def _on_new_doc():
        props['setDoc'](Datastore.instance.new_doc())
        set_show_edit(True)

    def _on_list():
        props['setDoc']('')
        set_show_edit(False)

    def _on_info():
        props['setDoc']('info')
        set_show_edit(False)

    def _on_edit():
        set_show_edit(True)

    buttons = [
        (button, {'onMsg': _on_new_doc}, '+ doc'),
        (button, {'onMsg': _on_list}, 'list'),
        (button, {'onMsg': _on_info}, 'info'),
    ]

    elems = []
    if len(doc_keys) == 0 or props['doc'] == 'info':
        elems.append((info,))
    elif props['doc'] != '':
        doc = Datastore.instance.get_doc(props['doc'])
        if show_edit:
            def _on_save_edits(msg, content):
                Datastore.instance.set_doc(props['doc'], content['doc-content'])
                set_show_edit(False)

            elems.append((form, {'id': 'edit-document', 'onMsg': _on_save_edits},[
                (text_area, {
                    'id': 'doc-content',
                    'className': 'editor-text-area',
                    'label': 'editor',
                    'default': doc['markdown'],
                }),
                (button, {'id': 'save'}, 'save'),
            ]))
        else:
            elems.append((doc_content, {'doc': doc}))
            buttons.append((button, {'onMsg': _on_edit}, 'edit'))

    else:
        def _on_doc(doc_key: str):
            def _impl():
                props['setDoc'](doc_key)
            return _impl

        for key in doc_keys:
            doc = Datastore.instance.get_doc(key)
            elems.append((doc_stripe, {'doc': doc, 'onClick': _on_doc(key)}))

    return pyreact_sprite(
        ('div', {'className': 'docs', 'class': STYLE}, [
            ('div', 'toolbar', buttons),
            ('div', 'content', elems),
        ])
    )
