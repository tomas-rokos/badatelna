from scientists.e_tokenizer.document_parser import document_parser


class Warehouse:

    def __init__(self):
        self.cache = {}

    def _set_document(self, document_id: str, markdown: str):
        props, tokens = document_parser(markdown)
        self.cache[document_id] = {
            'id': document_id,
            'markdown': markdown,
            'props': props,
            'tokens': tokens,
        }

    def get_doc_keys(self):
        return self.cache.keys()

    def get_doc(self, doc_key: str):
        return self.cache[doc_key]
