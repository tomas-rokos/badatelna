import json

import pytest

from scientists.n_timeliner.timeliner import timeliner


SAMPLE = [
    {
        "source": {
            "doc": "data/neptun",
            "from": 78,
            "to": 89
        },
        "keys": [
            "datetime:1965",
            "event:akce-neptun",
            "tag:sample",
            "tag:stb",
            "tag:wikipedia"
        ]
    },
    {
        "source": {
            "doc": "data/neptun",
            "from": 176,
            "to": 186
        },
        "keys": [
            "datetime:1963",
            "event:akce-neptun",
            "tag:sample",
            "tag:stb",
            "tag:wikipedia"
        ]
    },
    {
        "source": {
            "doc": "data/neptun",
            "from": 100,
            "to": 101
        },
        "keys": [
            "event:akce-neptun",
            "tag:sample",
            "tag:stb",
            "tag:wikipedia"
        ]
    },
    {
        "source": {
            "doc": "data/neptun",
            "from": 273,
            "to": 286
        },
        "keys": [
            "city:prague-cz",
            "datetime:1964-06-20",
            "event:akce-neptun",
            "place:sumava",
            "tag:sample",
            "tag:stb",
            "tag:wikipedia"
        ]
    },
]


def test_empty_streaks():
    assert timeliner([]) == []


def test_basic_sort():
    assert timeliner(SAMPLE) == [
        {
            'keys': [
                'datetime:1963',
                'event:akce-neptun',
                'tag:sample',
                'tag:stb',
                'tag:wikipedia'
            ],
            'source': {'doc': 'data/neptun', 'from': 176, 'to': 186},
            'timeline': '1963'
        },
        {
            'keys': [
                'city:prague-cz',
                'datetime:1964-06-20',
                'event:akce-neptun',
                'place:sumava',
                'tag:sample',
                'tag:stb',
                'tag:wikipedia'
            ],
            'source': {'doc': 'data/neptun', 'from': 273, 'to': 286},
            'timeline': '1964-06-20'},
        {
            'keys': [
                'datetime:1965',
                'event:akce-neptun',
                'tag:sample',
                'tag:stb',
                'tag:wikipedia'
            ],
            'source': {'doc': 'data/neptun', 'from': 78, 'to': 89},
            'timeline': '1965'
        },
    ]


def test_immutable():
    result = timeliner(SAMPLE)
    assert result[0]['timeline'] == '1963'
    assert all(item.get('timeline') is None for item in SAMPLE)


@pytest.mark.parametrize("filename", ('kamen', 'neptun'))
def test_data_file(filename: str):
    with open(f'data/{filename}.streaks.json') as f:
        source = json.load(f)
    with open(f'data/{filename}.timeline.json') as f:
        expected = json.load(f)
    result = timeliner(source)
    if result != expected:
        with open(f'data/{filename}.timeline.json', 'w') as f:
            json.dump(result, f)
    assert result == expected
