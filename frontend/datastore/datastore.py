from crassum.js.dict_keys import dict_keys
from crassum.js.http_request import async_http_request
from crassum.utils.random_string import random_string
from scientists.warehouse import Warehouse


TEMPLATE = '''* title: titulek
* url: https://some.cz
* tags: abc, def
---
# Header
abc def'''


class Datastore(Warehouse):
    instance = None

    def __init__(self):
        Warehouse.__init__(self)
        Datastore.instance = self

    async def load_docs(self):
        if window.location.href.find('localhost') != -1:
            for key in ['data/neptun', 'data/kamen']:
                content = await async_http_request(f'/{key}.md', 'text')
                self._set_document(key, content)
        else:
            keys = [key for key in dict_keys(localStorage)
                    if key.startswith('doc.')]
            for key in keys:
                content = localStorage.getItem(key)
                self._set_document(key, content)

    def new_doc(self) -> str:
        new_key = f'doc.{random_string(10)}'
        localStorage.setItem(new_key, TEMPLATE)
        self._set_document(new_key, TEMPLATE)
        return new_key

    def set_doc(self, doc_key: str, markdown: str):
        localStorage.setItem(doc_key, markdown)
        self._set_document(doc_key, markdown)
