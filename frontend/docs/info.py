from crassum.js.http_request import async_http_request
from crassum.js.navigation import navigate
from crassum.react.materializecss.button import button
from crassum.react.pyreact_functional import pyreact_sprite, use_effect, use_state


# language=css
STYLE = '''
:host {
    padding: 10px;
}

:host h1 {
    font-size: 200%;
    margin: 0;
}

'''


async def _import():
    for doc in ('data/neptun', 'data/kamen'):
        await _import_document(doc)
    window.location.href = '?doc=data/kamen'


async def _import_document(name: str):
    console.log('importing', name)
    content = await async_http_request(f'/{name}.md', 'text')
    localStorage.setItem('doc.' + name, content)
    console.log('fully imported')


def info(props):
    def _on_import():
        _import()

    return pyreact_sprite(('div', {'class': STYLE}, [
        ('h1', None, 'Badatelna'),
        ('p', None, 'Vítejte v aplikace Badatelna, určené pro ukládání a '
                    'analýzu dokumentů.'),
        ('p', None, 'Pokračováním v užívání souhlasíte s následujícím ujednáním:'),
        ('p', None, '* všechna data vložená uživatelem zůstávají výhradně v prohlížeči'),
        ('p', None, '* NIC, ani statistiky užití se NIKAM neposílají'),
        ('p', None, '* uložená data mohou být při přechodu na novější verzi smazána'),
        ('br', ),
        ('p', None, 'Na serveru jsem pro Vás připravil pár ukázkových '
                    'dokumentů. Stáhnout si je můžete kdykoliv stisknutím '
                    'tlačítka níže. Pokud je již máte, budou smazány '
                    'veškeré Vaše modifikace a nahrána aktuální verze ze '
                    'serveru.'),
        (button, {'onMsg': _on_import}, 'Stáhnout ukázkové dokumenty ze serveru'),
        ('br',),
        ('p', None, [
            'Celá aplikace je napsána v jazyce ',
            ('a', {'href': 'https://www.python.org'}, 'Python'),
            ' a na frontendu byly využity knihovny ',
            ('a', {'href': 'https://www.reactjs.org'}, 'ReactJS'),
            ' (vizualizace) a ',
            ('a', {'href': 'https://www.transcrypt.org'}, 'Transcrypt'),
            ' (transpiler Python -> JS).',
        ]),
    ]))
