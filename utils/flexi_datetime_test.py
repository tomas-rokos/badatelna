from utils.flexi_datetime import FlexiDatetime


def test_simple_creation():
    fdt = FlexiDatetime()
    assert fdt.day is None


def test_assign_values():
    fdt = FlexiDatetime()
    fdt.year = 2009
    fdt.month = 6
    assert fdt.day is None
    assert fdt.month == 6
    assert fdt.year == 2009


def test_repr_date():
    fdt = FlexiDatetime()
    assert repr(fdt) == ''
    fdt = FlexiDatetime()
    fdt.year = 2019
    assert repr(fdt) == '2019'
    fdt = FlexiDatetime()
    fdt.year = 2019
    fdt.month = 6
    assert repr(fdt) == '2019-06'
    fdt = FlexiDatetime()
    fdt.year = 2019
    fdt.month = 6
    fdt.day = 20
    assert repr(fdt) == '2019-06-20'
    fdt = FlexiDatetime()
    fdt.month = 6
    fdt.day = 20
    assert repr(fdt) == '????-06-20'
    fdt = FlexiDatetime()
    fdt.day = 20
    assert repr(fdt) == '????-??-20'
    fdt = FlexiDatetime()
    fdt.month = 6
    assert repr(fdt) == '????-06'
    fdt = FlexiDatetime()
    fdt.year = 2019
    fdt.day = 20
    assert repr(fdt) == '2019-??-20'


def test_repr_time():
    fdt = FlexiDatetime()
    fdt.hour = 12
    assert repr(fdt) == '12:??'
    fdt = FlexiDatetime()
    fdt.hour = 12
    fdt.minute = 33
    assert repr(fdt) == '12:33'
    fdt = FlexiDatetime()
    fdt.hour = 12
    fdt.minute = 33
    fdt.second = 45
    assert repr(fdt) == '12:33:45'
    fdt = FlexiDatetime()
    fdt.minute = 33
    fdt.second = 45
    assert repr(fdt) == '??:33:45'
    fdt = FlexiDatetime()
    fdt.minute = 33
    assert repr(fdt) == '??:33'
    fdt = FlexiDatetime()
    fdt.second = 45
    assert repr(fdt) == '??:??:45'


def test_repr_datetime():
    fdt = FlexiDatetime()
    fdt.year = 2019
    fdt.month = 6
    fdt.day = 20
    fdt.hour = 13
    fdt.minute = 34
    fdt.second = 59
    assert repr(fdt) == '2019-06-20T13:34:59'
    fdt = FlexiDatetime()
    fdt.day = 20
    fdt.minute = 34
    fdt.second = 59
    assert repr(fdt) == '????-??-20T??:34:59'
