

def construct_rss_guid(link: str) -> str:
    parts = link.split('/')[3:]
    parts = [part.strip().replace('.', '-')
             for part in parts if part.strip() != '']
    return '-'.join(parts)
