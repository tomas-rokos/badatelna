from utils.csv_string import list_to_csv_string, csv_string_to_list


def test_list_to_csv_string():
    result = list_to_csv_string([['a', 'b', 23, 'd'], [',', '.', 23.45, 'x']])
    assert result == 'a,b,23,d\n",",.,23.45,x'


def test_csv_string_to_list():
    result = csv_string_to_list('a,b,23,d\n",",.,23.45,x')
    assert result == [['a', 'b', 23, 'd'], [',', '.', 23.45, 'x']]
