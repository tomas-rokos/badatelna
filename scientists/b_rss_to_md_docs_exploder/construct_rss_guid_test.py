from scientists.b_rss_to_md_docs_exploder.construct_rss_guid import \
    construct_rss_guid


def test_basic_url():
    to_test = {
        'https://www.abc.cz/page-of-interest.html': 'page-of-interest-html'
    }
    for url, guid in to_test.items():
        assert construct_rss_guid(url) == guid
