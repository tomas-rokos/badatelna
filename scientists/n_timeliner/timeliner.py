from crassum.js.array import sort_array_by_lambda


def timeliner(streaks: list) -> list:
    result = []
    for item in streaks:
        datetimes = [key for key in item['keys']
                     if key.startswith('datetime:')]
        if len(datetimes) == 0:
            continue
        first_one = datetimes[0]
        result_item = dict(item)
        result_item['timeline'] = first_one[9:]
        result.append(result_item)

    sort_array_by_lambda(result, _sorting)

    return result


def _sorting(a: dict, b: dict) -> int:
    if a['timeline'] == b['timeline']:
        return 0
    return -1 if a['timeline'] < b['timeline'] else 1
