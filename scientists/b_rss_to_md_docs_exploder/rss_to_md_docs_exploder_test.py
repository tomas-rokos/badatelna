import os

import pytest

from scientists.b_rss_to_md_docs_exploder.rss_to_md_docs_exploder import \
    rss_to_md_docs_exploder


@pytest.mark.parametrize("filename", ('sample', 'idnes', 'ceskenoviny'))
def test_sample(filename: str):
    script_dir = os.path.dirname(__file__)
    with open(os.path.join(script_dir, f'test_data/{filename}.xml')) as f:
        content = f.read()

    result = rss_to_md_docs_exploder(content.encode('utf-8'))

    final_text = ('\n' + (30 * '.') + '\n').join(result)

    with open(os.path.join(script_dir, f'test_data/{filename}_docs.txt')) as f:
        expected = f.read()
    assert final_text == expected
