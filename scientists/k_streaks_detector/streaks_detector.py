from scientists.warehouse import Warehouse


def streaks_detector(w: Warehouse) -> list:
    result = []

    for doc_key in w.get_doc_keys():
        doc = w.get_doc(doc_key)
        tags = [tag if ':' in tag else f'tag:{tag}'
                for tag in doc['props'].get('tags', [])]
        if doc['props'].get('type') != 'article':
            result.append({'source': {'doc': doc_key}, 'keys': list(tags)})
            continue
        curr_path = None
        curr_result = {}
        ln = len(doc['tokens'])
        for token_idx in range(ln + 1):
            token = doc['tokens'][token_idx] if token_idx < ln else ('', '', '', '',)
            act_path = token[3]
            if bool(curr_result):
                if act_path.startswith(curr_path):
                    if len(token) == 5 and token[4] not in curr_result['keys']:
                        curr_result['keys'].append(token[4])
                    continue
                curr_result['source']['to'] = token_idx - 1
                result.append(curr_result)
                curr_result = {}
            parts = act_path.split('.')
            if 'strong' not in parts:
                continue
            idx = parts.index('strong')
            curr_result = {
                'source': {
                    'doc': doc_key,
                    'from': token_idx,
                },
                'keys': list(tags),
            }
            curr_path = '.'.join(parts[:idx + 2])

    return result
