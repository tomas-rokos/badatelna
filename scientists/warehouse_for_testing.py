from scientists.warehouse import Warehouse


class WarehouseForTesting(Warehouse):
    def __init__(self, load_samples=True):
        Warehouse.__init__(self)
        if not load_samples:
            return
        for filename in ('data/neptun', 'data/kamen'):
            self.load_document(filename)

    def add_document(self, name: str, markdown: str):
        self._set_document(name, markdown)

    def load_document(self, filename: str):
        with open(f'{filename}.md') as f:
            content = f.read()
        self._set_document(filename, content)
