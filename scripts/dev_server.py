#!/usr/bin/env python3
from crassum.scripts.dev_server import start_server

start_server([
    'frontend/components/*.py',
    'frontend/datastore/*.py',
    'frontend/docs/*.py',
    'frontend/workplace/*.py',
    'scientists/*.py',
    'scientists/**/*.py',
    'utils/*.py',
])
