* type: article
* title: Akce Kámen (wikipedia)
* url: https://cs.wikipedia.org/wiki/Akce_Kámen
* tags: stb, wikipedia, sample
---
# Akce Kámen

Akce Kámen (občas chybně uváděná jako Akce Kameny) byla provokační metoda, 
kterou využívala StB proti těm československým občanům, kteří se po únoru 
1948 snažili odejít bez vědomí komunistických úřadů do zahraničí. Konkrétně 
šlo o ty, kteří použili nejběžnější cestu, tedy pěšky přes státní hranici do 
americké okupační zóny v Německu.


## Stručný popis
Akce Kámen byla provokační metoda StB cílená na ty československé občany, 
kteří se po vítězném Únoru 1948 pokusili ilegálně přejít pěšky státní hranici
s Německem a dostat se do americké okupační zóny. Za tímto účelem byla
(ještě na území Československa) zřízena fiktivní státní hranice.
Po jejím „překročení“ zastavila uprchlíky falešná hlídka německé pohraniční
stráže. Následně byli takto zadržení emigranti dovedeni do falešné úřadovny 
polního důstojníka americké vojenské kontrarozvědky. Úřadovna byla realisticky
zařízena tak, aby ve zde zpravodajsky vyslýchaných uprchlících vzbuzovala
reálnou iluzi kanceláře americké vojenské kontrarozvědky. Cílem fiktivního
výslechu bylo vylákat z emigranta výpovědi o jeho činnosti proti režimu,
o jeho spolupracovnících žijících dosud na území Československa a přimět
jej případně k tomu, aby dobrovolně odevzdal i (měl-li nějaké) vlastněné 
zbraně. Po skončení fiktivního výslechu byli uprchlíci odváděni do
sběrného tábora. Během této cesty došlo k jejich „zabloudění“ na
území Československa a jejich zatčení. Nebo během cesty do tábora byli
na „německém území“ přepadení hlídkou SNB, zatčeni a „uneseni“ zpět do
Československa. Informace lstí získané od uprchlíka ve fiktivní úřadovně
americké vojenské kontrarozvědky byly následně použity při procesu směřujícím
k jeho odsouzení k vysokému trestu odnětí svobody. StB dále využila i 
informace, které emigrant uvedl o svých spolupracovnících žijících dosud 
na území Československa. Příslušníci StB i jejich tajní spolupracovníci 
se při akci Kámen mnohdy obohacovali tím, že okrádali zatčené a mnohé z nich 
potom i zavraždili.

## Podrobný popis
### Způsob provedení

Akce Kámen začala podstrčením tajných spolupracovníků, konkrétně příslušníků 
StB v roli převaděčů přes hranici, zájemcům o emigraci z Československa. Tyto
osoby byly dovezeny do pohraničí přímo z Prahy ve vozidlech StB nebo byly v 
oblasti vyzvednuty na dohodnutém místě a posléze dovedeny na „státní hranici“,
která byla ale jen zinscenovaná, v bezpečné vzdálenosti od skutečné hranice.
Přitom StB využívala noční tmu a neznalost terénu převáděných osob.

Pro účely akce Kámen byla využívána například lokalita v okolí Všerub u
Domažlic. Zde u zinscenované „hranice“ došlo buď k předstírání dopadení 
celé skupiny pohraniční hlídkou, při kterém se podařilo převaděčům náhodou
„uniknout“ do zahraničí, nebo akce pokračovala složitým divadlem. Uprchlíci 
po „překročení“ fiktivní státní hranice byli zastaveni falešnou hlídkou
německé pohraniční policie a dovedeni do falešné úřadovny polního důstojníka
americké vojenské kontrarozvědky.

Zde byli v naaranžovaném prostředí údajné americké kanceláře podrobeni 
zpravodajskému výslechu, při kterém vypovídali o své činnosti proti režimu,
o spolupracovnících při této činnosti žijících dosud v ČSR a předali případné 
zbraně. Po výslechu na úřadovně uprchlíci odcházeli do tábora, přitom
však „zabloudili“ na území ČSR, nebo byli na „německém území“ přepadení
hlídkou SNB, byli zatčeni a „uneseni“ do ČSR.

Poté byli „uprchlíci“ vyšetřováni StB s využitím informací, které o sobě 
sami dobrovolně vypověděli na fingované úřadovně a odsuzováni k vysokým
trestům odnětí svobody. Informace získané při akci pak StB dále využívala
i proti lidem dosud žijícím v Československu. Příslušníci StB i jejich tajní
spolupracovníci se při akcích Kámen obohacovali okrádáním zatčených, někdy
na vlastní pěst organizovali i soukromé úniky tohoto druhu, přičemž
docházelo i k vraždám uprchlíků.

### Autoři akce Kámen
Podle dosavadních poznatků byly do akce Kámen zapojeny úřadovny StB především
v Praze, Karlových Varech, Aši, Chebu, Mariánských Lázních, Plzni, Klatovech
a Brně. V roce 2013 byl policii obviněn jako strůjce 92letý Evžen Abrahamovič
(krycí jméno Dr. Breza), který ale rok na to zemřel, dřív než byl souzen.
Zároveň byl obviněn i jeho komplic Emil Orovan, po kterém pátrá Interpol.
V roli amerického důstojníka často vystupoval agent IV. Sektoru
skupiny BAa Velitelství StB Amon Tomašoff.

Počet provedených akcí Kámen jde pravděpodobně do stovek, ale dosud se podařilo
doložit a zdokumentovat příběhy jen čtyř desítek obětí. Dosud nebyly objeveny
informace StB obecnější povahy nebo souhrnné vyhodnocení.
