from scientists.e_tokenizer.token_type_enum import TokenType
from scientists.e_tokenizer.text import text, breakers


HEADER_STARTS = {
    '#': TokenType.HEADER1,
    '##': TokenType.HEADER2,
    '###': TokenType.HEADER3,
}


def markup(txt: str):
    tokenized: list = []

    act_string = ''
    shift_act_start_pos = False
    act_start_pos = 0

    lines = txt.split('\n')
    lines.append('')
    curr_line_pos = 0
    for line in lines:
        stripped_line = line.strip()
        if len(stripped_line) == 0:
            if len(act_string) != 0:
                _add_to_results(tokenized, text(act_string), act_start_pos,
                                TokenType.PARAGRAPH, txt)
                act_string = ''
            shift_act_start_pos = True
        else:
            if len(act_string) != 0:
                act_string += '\n' + line
            else:
                for header_md, header_type in HEADER_STARTS.items():
                    ln = len(header_md)
                    if not stripped_line.startswith(header_md):
                        continue
                    if ord(stripped_line[ln]) not in breakers:
                        continue
                    offset = line.find(header_md)
                    _add_to_results(
                        tokenized, text(stripped_line[ln + 1:]),
                        act_start_pos + ln + 1 + offset, header_type, txt)
                    shift_act_start_pos = True
                    break
                if not shift_act_start_pos:
                    act_string = line
                    act_start_pos = curr_line_pos

        curr_line_pos = curr_line_pos + len(line) + 1
        if shift_act_start_pos:
            act_start_pos = curr_line_pos
            shift_act_start_pos = False
    return tokenized


def _add_to_results(tokenized: list, block_tokens: list, offset: int,
                    block_type: str, txt: str):
    shifted_tokens = [
        (block_token[0] + offset, block_token[1], block_token[2],
         block_type + '.' + str(offset))
        for block_token in block_tokens
    ]
    shifted_tokens = _collapse_strong(shifted_tokens)
    shift_tokens = _collapse_links(shifted_tokens, txt)
    tokenized.extend(shift_tokens)


def _collapse_strong(block_tokens: list) -> list:
    strong_idxs = [idx for idx, token in enumerate(block_tokens)
                   if token[1] == '**']
    to_drop = []
    while len(strong_idxs) > 1:
        start = strong_idxs.pop(0)
        end = strong_idxs.pop(0)
        nested_path = '.strong.' + str(block_tokens[start+1][0])
        for idx in range(start + 1, end):
            prev = list(block_tokens[idx])
            prev[3] = prev[3] + nested_path
            block_tokens[idx] = tuple(prev)
        to_drop.append(start)
        to_drop.append(end)
    return [token for idx, token in enumerate(block_tokens) if idx not in to_drop]


def _collapse_links(block_tokens: list, txt: str) -> list:
    start_bracket = middle_brackets = -1
    to_drop = []
    for idx, token in enumerate(block_tokens):
        if token[1] == '[':
            start_bracket = idx
            middle_brackets = -1
        elif token[1] == '](':
            middle_brackets = idx
        if token[1] != ')':
            continue
        if start_bracket == -1 or middle_brackets == -1:
            continue

        to_drop.append(block_tokens[start_bracket][0])
        for drop_token in block_tokens[middle_brackets:idx+1]:
            to_drop.append(drop_token[0])

        link = txt[block_tokens[middle_brackets + 1][0]:
                   block_tokens[idx][0]]
        nested_path = '.a.' + str(block_tokens[start_bracket+1][0])
        for tkn_idx in range(start_bracket+1, middle_brackets):
            prev = list(block_tokens[tkn_idx])
            prev.append(link)
            prev[3] = prev[3] + nested_path
            block_tokens[tkn_idx] = tuple(prev)
        start_bracket = middle_brackets = -1
    return [token for token in block_tokens if token[0] not in to_drop]
