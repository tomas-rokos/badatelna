from scientists.e_tokenizer.markup import markup


def test_empty():
    assert markup('') == []
    assert markup('   ') == []
    assert markup(' \n  \n  ') == []


def test_one_paragraph():
    assert markup('a a b\n b b c') == \
        [(0, 'a', 'w', 'p.0'),
         (2, 'a', 'w', 'p.0'),
         (4, 'b', 'w', 'p.0'),
         (7, 'b', 'w', 'p.0'),
         (9, 'b', 'w', 'p.0'),
         (11, 'c', 'w', 'p.0')]
    assert markup(' \n  \na a b\n b b c\n  \n  ') == \
        [(5, 'a', 'w', 'p.5'),
         (7, 'a', 'w', 'p.5'),
         (9, 'b', 'w', 'p.5'),
         (12, 'b', 'w', 'p.5'),
         (14, 'b', 'w', 'p.5'),
         (16, 'c', 'w', 'p.5')]


def test_two_paragraphs():
    assert markup('a a b\n b b c\n\nd e f ') == \
        [(0, 'a', 'w', 'p.0'),
         (2, 'a', 'w', 'p.0'),
         (4, 'b', 'w', 'p.0'),
         (7, 'b', 'w', 'p.0'),
         (9, 'b', 'w', 'p.0'),
         (11, 'c', 'w', 'p.0'),
         (14, 'd', 'w', 'p.14'),
         (16, 'e', 'w', 'p.14'),
         (18, 'f', 'w', 'p.14')]
    assert markup(' \n  \na a b\n b b c\n  \nd e f\n \n  \n  ') == \
        [(5, 'a', 'w', 'p.5'),
         (7, 'a', 'w', 'p.5'),
         (9, 'b', 'w', 'p.5'),
         (12, 'b', 'w', 'p.5'),
         (14, 'b', 'w', 'p.5'),
         (16, 'c', 'w', 'p.5'),
         (21, 'd', 'w', 'p.21'),
         (23, 'e', 'w', 'p.21'),
         (25, 'f', 'w', 'p.21')]


def test_header_1():
    assert markup('# header') == [(2, 'header', 'w', 'h1.2')]
    assert markup('\n  # header  \n \n  ') == [(5, 'header', 'w', 'h1.5')]


def test_header_2():
    assert markup('## header') == [(3, 'header', 'w', 'h2.3')]
    assert markup('\n ## header \n  \n') == [(5, 'header', 'w', 'h2.5')]


def test_header_3():
    assert markup('### header') == [(4, 'header', 'w', 'h3.4')]
    assert markup('\n  ### header \n  \n') == [(7, 'header', 'w', 'h3.7')]


def test_link():
    assert markup('a [b](urn)\nb') == \
        [(0, 'a', 'w', 'p.0'),
         (3, 'b', 'w', 'p.0.a.3', 'urn'),
         (11, 'b', 'w', 'p.0')]


def test_link_multi_word():
    assert markup('a [b c](urn)\nb') == \
        [(0, 'a', 'w', 'p.0'),
         (3, 'b', 'w', 'p.0.a.3', 'urn'),
         (5, 'c', 'w', 'p.0.a.3', 'urn'),
         (13, 'b', 'w', 'p.0')]


def test_link_with_numbers_with_leading_zeros():
    assert markup('a [b c](urn:002:019)\nb') == \
        [(0, 'a', 'w', 'p.0'),
         (3, 'b', 'w', 'p.0.a.3', 'urn:002:019'),
         (5, 'c', 'w', 'p.0.a.3', 'urn:002:019'),
         (21, 'b', 'w', 'p.0')]


def test_multi_link():
    assert markup('a [b c](urn)\nb e \n\n  r [s t](urn:234:566)\n ') == \
        [(0, 'a', 'w', 'p.0'),
         (3, 'b', 'w', 'p.0.a.3', 'urn'),
         (5, 'c', 'w', 'p.0.a.3', 'urn'),
         (13, 'b', 'w', 'p.0'),
         (15, 'e', 'w', 'p.0'),
         (21, 'r', 'w', 'p.19'),
         (24, 's', 'w', 'p.19.a.24', 'urn:234:566'),
         (26, 't', 'w', 'p.19.a.24', 'urn:234:566')]


def test_strong():
    assert markup('a **b c d**\ne') == \
        [(0, 'a', 'w', 'p.0'),
         (4, 'b', 'w', 'p.0.strong.4'),
         (6, 'c', 'w', 'p.0.strong.4'),
         (8, 'd', 'w', 'p.0.strong.4'),
         (12, 'e', 'w', 'p.0')]


def test_strong_with_links():
    assert markup('a **b c [f g](urn) d**\ne') == \
        [(0, 'a', 'w', 'p.0'),
         (4, 'b', 'w', 'p.0.strong.4'),
         (6, 'c', 'w', 'p.0.strong.4'),
         (9, 'f', 'w', 'p.0.strong.4.a.9', 'urn'),
         (11, 'g', 'w', 'p.0.strong.4.a.9', 'urn'),
         (19, 'd', 'w', 'p.0.strong.4'),
         (23, 'e', 'w', 'p.0')]
