import json

import pytest

from scientists.k_streaks_detector.streaks_detector import streaks_detector
from scientists.warehouse_for_testing import WarehouseForTesting


def test_empty():
    w = WarehouseForTesting(load_samples=False)
    assert streaks_detector(w) == []


def test_article_no_streaks():
    doc = '''* type: article
* title: něco
* url: https://some.url
* tags: abc, def
---
# Prázdnej article 
Tohle je jednoduchý article type document, ve
kterém nejsou žádné streaks!
'''
    w = WarehouseForTesting(load_samples=False)
    w.add_document('sample', doc)
    assert streaks_detector(w) == []


def test_article_single_streak():
    doc = '''* type: article
* title: něco
* url: https://some.url
* tags: abc, def
---
# Article!
Tady v tom articlu už **jeden streak je ** A 
musí být nalezen.

Od toho je to tu tento test.
'''
    w = WarehouseForTesting(load_samples=False)
    w.add_document('sample', doc)
    assert streaks_detector(w) == [
        {
            'source': {
                'doc': 'sample',
                'from': 6,
                'to': 8,
            },
            'keys': ['tag:abc', 'tag:def'],
        }
    ]


def test_streak_document():
    doc = '''* title: něco
* url: https://some.url
* tags: abc, def
---
Toto je automatický streak.
'''
    w = WarehouseForTesting(load_samples=False)
    w.add_document('sample', doc)
    assert streaks_detector(w) == [
        {
            'source': {'doc': 'sample'},
            'keys': ['tag:abc', 'tag:def'],
        }
    ]


def test_streak_no_props_document():
    doc = 'Toto je automatický streak.'
    w = WarehouseForTesting(load_samples=False)
    w.add_document('sample', doc)
    assert streaks_detector(w) == [
        {
            'source': {'doc': 'sample'},
            'keys': [],
        }
    ]


@pytest.mark.parametrize("filename", ('kamen', 'neptun'))
def test_data_file(filename: str):
    w = WarehouseForTesting(load_samples=False)
    w.load_document(f'data/{filename}')
    with open(f'data/{filename}.streaks.json') as f:
        expected = json.load(f)
    result = streaks_detector(w)
    if result != expected:
        with open(f'data/{filename}.streaks.json', 'w') as f:
            json.dump(result, f)
    assert result == expected
