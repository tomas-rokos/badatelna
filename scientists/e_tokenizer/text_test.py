from scientists.e_tokenizer.text import text, TokenType


def test_empty():
    assert text('') == []


def test_one_token():
    assert text('test') == [(0, 'test', TokenType.WORD)]
    assert text('456') == [(0, 456, TokenType.NUMBER)]


def test_more_tokens():
    assert text('test few mores') == [(0, 'test', TokenType.WORD),
                                      (5, 'few', TokenType.WORD),
                                      (9, 'mores', TokenType.WORD)]
    assert text('test 2 mores') == [(0, 'test', TokenType.WORD),
                                    (5, 2, TokenType.NUMBER),
                                    (7, 'mores', TokenType.WORD)]


def test_white_spacing():
    assert text('  This is    a  white space\n  test') == \
        [(2, 'This', TokenType.WORD),
         (7, 'is', TokenType.WORD),
         (13, 'a', TokenType.WORD),
         (16, 'white', TokenType.WORD),
         (22, 'space', TokenType.WORD),
         (30, 'test', TokenType.WORD)]


def test_more_token_types():
    assert text('This is 2.3 test\n of x\' tokens') == \
        [(0, 'This', TokenType.WORD),
         (5, 'is', TokenType.WORD),
         (8, 2, TokenType.NUMBER),
         (9, '.', TokenType.PUNCTUATION),
         (10, 3, TokenType.NUMBER),
         (12, 'test', TokenType.WORD),
         (18, 'of', TokenType.WORD),
         (21, 'x', TokenType.WORD),
         (22, "'", TokenType.UNKNOWN),
         (24, 'tokens', TokenType.WORD)]


def test_trailing_brackets():
    assert text(' abc (def)') == \
        [(1, 'abc', TokenType.WORD),
         (5, '(', TokenType.PUNCTUATION),
         (6, 'def', TokenType.WORD),
         (9, ')', TokenType.PUNCTUATION)]
